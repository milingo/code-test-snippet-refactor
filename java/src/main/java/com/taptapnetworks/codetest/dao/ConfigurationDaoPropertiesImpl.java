package com.taptapnetworks.codetest.dao;

import java.util.Properties;

/**
 * DAO for configuration items such as smpp host....
 * 
 * @author miguel
 * 
 */
public class ConfigurationDaoPropertiesImpl implements ConfigurationDao {

    private PropertiesHolder propertiesHolder;

    /**
     * {@inheritDoc}
     */
    public String getConfigurationItem(String key) throws ConfigurationItemNotFound {
        Properties properties = propertiesHolder.getConfigurationProperties();
        String item;
        if (properties != null) {
            item = properties.getProperty(key);
            if (item != null) {
                return item;
            }
        }
        // If we got here, something went wrong
        throw new ConfigurationItemNotFound();
    }

    /**
     * {@inheritDoc}
     */
    public void setPropertiesHolder(PropertiesHolder propertiesHolder) {
        this.propertiesHolder = propertiesHolder;
    }

}
