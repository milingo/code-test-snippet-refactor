package com.taptapnetworks.codetest.dao;

/**
 * Configuration DAO. For now we are only having into account the read method
 * but we should implement the rest of CRUD operations.
 * 
 * @author miguel
 * 
 */
public interface ConfigurationDao {

    public static final String SMPP_HOST = "smpp_host";
    public static final String SMPP_PORT = "smpp_port";
    public static final String MAIL_SERVER = "mail_server";
    public static final String SMPP_USERNAME = "smpp_username";
    public static final String SMPP_PASSWORD = "smpp_password";

    /**
     * Gets the value of the item with the given key.
     * 
     * @param key
     * @return value
     */
    public String getConfigurationItem(String key) throws ConfigurationItemNotFound;

    /**
     * This is just for setter injection.
     * 
     * @param propertiesHolder
     */
    public void setPropertiesHolder(PropertiesHolder propertiesHolder);

}
