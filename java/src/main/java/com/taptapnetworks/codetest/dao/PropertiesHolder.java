package com.taptapnetworks.codetest.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * The next step for this holder is to cache the properties i.e. on a HashTable.
 * 
 * @author miguel
 * 
 */
public class PropertiesHolder {

    private static final String CONFIGURATION_NAMESPACE = "/com/taptapnetworks/codetest/configuration.properties";

    /**
     * Get the configuration properties.
     * 
     * @return
     */
    public Properties getConfigurationProperties() {
        Properties configurationProperties = new Properties();
        try {
            InputStream resource = this.getClass().getResourceAsStream(CONFIGURATION_NAMESPACE);
            if (resource != null) {
                configurationProperties.load(resource);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return configurationProperties;
    }

}
