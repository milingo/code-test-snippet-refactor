package com.taptapnetworks.codetest.communication;

import com.taptapnetworks.codetest.dao.ConfigurationDao;

/**
 * Factory for {@link Messenger}s.
 * 
 * @author miguel
 * 
 */
public interface MessengerFactory {

    /**
     * Returns a {@link Messenger} of the given {@link MessengerType}.
     * 
     * @param type
     * @return
     */
    public Messenger getMessenger(MessengerType type);

    /**
     * This is just for injection.
     * 
     * @param configurationDao
     */
    public void setConfigurationDao(ConfigurationDao configurationDao);

}
