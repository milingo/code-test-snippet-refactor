package com.taptapnetworks.codetest.communication;

/**
 * Mail Messager.
 * 
 * @author miguel
 * 
 */
public class SendMailConnection implements Messenger {

    /**
     * Constructor.
     * 
     * @param mailServer
     */
    public SendMailConnection(String mailServer) {
        super();
        System.out.println(String.format("Created SendMailConnection for server %s", mailServer));
    }

    /**
     * {@inheritDoc}
     */
    public void setupConnection() {
    }

    /**
     * {@inheritDoc}
     */
    public void send(Message message) {
        prepareMessage(message);
        System.out.println("Message sent!!!");
    }

    /**
     * {@inheritDoc}
     */
    public void close() {
        System.out.println("Mail connection closed");
    }

    /**
     * Prepare the message to be send.
     * 
     * @param message
     */
    private void prepareMessage(Message message) {
        System.out.println(String.format("Preparing message for %s", message.getTo()));
        System.out.println(String.format("Subject: %s", message.getSubject()));
        System.out.println(message.getMessage());
    }

}
