package com.taptapnetworks.codetest.communication;

import com.taptapnetworks.codetest.Utils;

/**
 * SMPP Messenger.
 * 
 * @author miguel
 * 
 */
public class SMPP implements Messenger {

    private final String host;
    private final String port;
    private final String username;
    private final String password;

    /**
     * Constructor.
     * 
     * @param host
     * @param port
     */
    public SMPP(String host, String port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    /**
     * {@inheritDoc}
     */
    public void setupConnection() {
        String hiddenPassword = Utils.hidePassword(password);
        System.out.println(String.format("Logged %s with password %s to %s:%s",
                username, hiddenPassword, host, port));
    }

    /**
     * {@inheritDoc}
     */
    public void send(Message message) {
        System.out.println(String.format("Sent message to %s", message.getTo()));
        System.out.println(message.getMessage());
    }

    /**
     * {@inheritDoc}
     */
    public void close() {
        System.out.println("Connection closed");
    }

}
