package com.taptapnetworks.codetest.communication;

import com.taptapnetworks.codetest.dao.ConfigurationDao;
import com.taptapnetworks.codetest.dao.ConfigurationItemNotFound;

/**
 * Factory for Messengers.
 * 
 * @author miguel
 * 
 */
public class MessengerFactoryImpl implements MessengerFactory {

    /**
     * The ConfigurationDao will provide the neede configuration items to create tge Messengers.
     */
    public ConfigurationDao configurationDao;

    /**
     * {@inheritDoc}
     */
    public Messenger getMessenger(MessengerType type) {
        try {
            switch (type) {
            case SMS:
                return new SMPP(configurationDao.getConfigurationItem(ConfigurationDao.SMPP_HOST),
                        configurationDao.getConfigurationItem(ConfigurationDao.SMPP_PORT),
                        configurationDao.getConfigurationItem(ConfigurationDao.SMPP_USERNAME),
                        configurationDao.getConfigurationItem(ConfigurationDao.SMPP_PASSWORD));
            case MAIL:
                return new SendMailConnection(
                        configurationDao.getConfigurationItem(ConfigurationDao.MAIL_SERVER));
            default:
                // TODO: Checked exception here
                break;
            }
        } catch (ConfigurationItemNotFound e) {
            // We should do something here, but since it is an unrecoverable error,
            // for now we will print the starcktrace
            e.printStackTrace();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void setConfigurationDao(ConfigurationDao configurationDao) {
        this.configurationDao = configurationDao;
    }

}
