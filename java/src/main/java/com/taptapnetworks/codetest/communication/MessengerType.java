package com.taptapnetworks.codetest.communication;

/**
 * This can be updated whenever we have a new Messenger.
 * 
 * @author miguel
 * 
 */
public enum MessengerType {
    SMS, MAIL;
}
