package com.taptapnetworks.codetest.communication;

/**
 * Common interface for all communication objects.
 * 
 * @author miguel
 * 
 */
public interface Messenger {

    /**
     * Boilerplate before sending the message.
     */
    public void setupConnection();

    /**
     * Sends the given message.
     * 
     * @param message the message
     */
    public void send(Message message);

    /**
     * Terminates teh communication.
     */
    public void close();

}
