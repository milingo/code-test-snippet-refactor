package com.taptapnetworks.codetest;

import com.taptapnetworks.codetest.communication.Message;
import com.taptapnetworks.codetest.communication.Messenger;
import com.taptapnetworks.codetest.communication.MessengerFactory;
import com.taptapnetworks.codetest.communication.MessengerType;

/**
 * The original class to refactor. We keep the send method but we use a factory
 * to create the possible Messengers. Thanks to the Factory we can add new
 * Messengers without affecting the client code.
 * 
 * @author miguel
 * 
 */
public class SnippetToRefactor {

    private MessengerFactory messengerFactory;

    /**
     * Sends the given message through the specified Messenger.
     * 
     * @param messengerType
     * @param message
     */
    public void send(MessengerType messengerType, Message message) {
        Messenger messenger = messengerFactory.getMessenger(messengerType);
        messenger.setupConnection();
        messenger.send(message);
        messenger.close();
    }

    /**
     * @param messengerFactory
     *            the messengerFactory to set
     */
    public void setMessengerFactory(MessengerFactory messengerFactory) {
        this.messengerFactory = messengerFactory;
    }

}
