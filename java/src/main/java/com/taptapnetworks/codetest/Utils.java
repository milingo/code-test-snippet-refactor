package com.taptapnetworks.codetest;

/**
 * 
 * @author miguel
 * 
 */
public class Utils {

    /**
     * hidePassword method
     * 
     * @param password
     * @return the password on a hidden format.
     */
    public static String hidePassword(String password) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < password.length(); i++) {
            sb.append("*");
        }
        return sb.toString();
    }

}
