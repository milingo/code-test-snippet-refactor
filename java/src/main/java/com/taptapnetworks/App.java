package com.taptapnetworks;

import com.taptapnetworks.codetest.SnippetToRefactor;
import com.taptapnetworks.codetest.communication.Message;
import com.taptapnetworks.codetest.communication.MessengerFactory;
import com.taptapnetworks.codetest.communication.MessengerFactoryImpl;
import com.taptapnetworks.codetest.communication.MessengerType;
import com.taptapnetworks.codetest.dao.ConfigurationDao;
import com.taptapnetworks.codetest.dao.ConfigurationDaoPropertiesImpl;
import com.taptapnetworks.codetest.dao.PropertiesHolder;

/**
 * Hello world!
 * 
 */
public class App {

    public static void main(String[] args) {
        // TODO: We should use a more elaborated IoC container, but this will do
        // for now.

        // PropertiesHolder will help up retrieve the configuration
        PropertiesHolder propertiesHolder = new PropertiesHolder();

        // Entry point for configuration items
        ConfigurationDao configurationDao = new ConfigurationDaoPropertiesImpl();
        configurationDao.setPropertiesHolder(propertiesHolder);

        // The MessengerFactory needs a ConfigurationDao to create its objects.
        MessengerFactory messengerFactory = new MessengerFactoryImpl();
        messengerFactory.setConfigurationDao(configurationDao);

        SnippetToRefactor sender = new SnippetToRefactor();
        sender.setMessengerFactory(messengerFactory);

        // We build the message
        Message message = new Message();
        message.setTo("Anyone");
        message.setMessage("Hello there!");
        message.setSubject("Testing mail");

        // .. and send it here trough the 2 types of Messengers we currently have.
        sender.send(MessengerType.SMS, message);
        sender.send(MessengerType.MAIL, message);
    }

}
