package com.taptapnetworks.codetest.dao;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Properties;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class ConfigurationDaoTest extends TestCase {

    /**
     * 
     */
    public void testGetConfigurationItem() {
        String itemKey = "key";
        String itemValue = "value";
        
        // Mock properties
        Properties properties = new Properties();
        properties.setProperty(itemKey, itemValue);
        // Mock propertiesHolder
        PropertiesHolder propertiesHolder = mock(PropertiesHolder.class);
        when(propertiesHolder.getConfigurationProperties()).thenReturn(properties);
        
        ConfigurationDao configurationDao = new ConfigurationDaoPropertiesImpl();
        configurationDao.setPropertiesHolder(propertiesHolder);

        try {
            String retrieved = configurationDao.getConfigurationItem(itemKey);
            assertEquals(itemValue, retrieved);
        } catch (ConfigurationItemNotFound e) {
            fail();
        }
    }

    /**
     * 
     */
    public void testNoConfigurationItem() {
        String itemKey = "key";
        String itemValue = "value";
        
        // Mock properties
        Properties properties = new Properties();
        properties.setProperty(itemKey, itemValue);
        // Mock propertiesHolder
        PropertiesHolder propertiesHolder = mock(PropertiesHolder.class);
        when(propertiesHolder.getConfigurationProperties()).thenReturn(properties);
        
        ConfigurationDao configurationDao = new ConfigurationDaoPropertiesImpl();
        configurationDao.setPropertiesHolder(propertiesHolder);

        try {
            configurationDao.getConfigurationItem("otherKey");
            fail();
        } catch (ConfigurationItemNotFound e) {
            // OK
        }
    }
}
