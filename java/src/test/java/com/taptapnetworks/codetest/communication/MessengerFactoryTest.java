package com.taptapnetworks.codetest.communication;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.taptapnetworks.codetest.dao.ConfigurationDao;
import com.taptapnetworks.codetest.dao.ConfigurationItemNotFound;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class MessengerFactoryTest extends TestCase {

    /**
     * 
     */
    public void testMessengerFactory() {
        String host = "key";
        String port = "1111";
        String username = "uu";
        String password = "pp";

        // Mock ConfigurationDao
        ConfigurationDao configurationDao = mock(ConfigurationDao.class);
        try {
            when(configurationDao.getConfigurationItem(ConfigurationDao.SMPP_HOST))
                    .thenReturn(host);
            when(configurationDao.getConfigurationItem(ConfigurationDao.SMPP_PORT))
                    .thenReturn(port);
            when(configurationDao.getConfigurationItem(ConfigurationDao.SMPP_USERNAME)).thenReturn(
                    username);
            when(configurationDao.getConfigurationItem(ConfigurationDao.SMPP_PASSWORD)).thenReturn(
                    password);
        } catch (ConfigurationItemNotFound e) {
            fail();
        }

        MessengerFactory messengerFactory = new MessengerFactoryImpl();
        messengerFactory.setConfigurationDao(configurationDao);
        
        Messenger smsMessenger = messengerFactory.getMessenger(MessengerType.SMS);
        assertNotNull(smsMessenger);
    }

}
