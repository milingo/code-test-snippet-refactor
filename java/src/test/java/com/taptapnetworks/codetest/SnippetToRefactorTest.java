package com.taptapnetworks.codetest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import junit.framework.TestCase;

import com.taptapnetworks.codetest.communication.Message;
import com.taptapnetworks.codetest.communication.Messenger;
import com.taptapnetworks.codetest.communication.MessengerFactory;
import com.taptapnetworks.codetest.communication.MessengerType;

/**
 * Unit test for simple App.
 */
public class SnippetToRefactorTest extends TestCase {

    /**
     * 
     */
    public void testSnippetToRefactor() {
        MessengerType type = MessengerType.MAIL;

        Message message = new Message();

        // Mock Messenger just to verify the calls
        Messenger messenger = mock(Messenger.class);
        // Mock ConfigurationDao
        MessengerFactory messengerFactory = mock(MessengerFactory.class);
        when(messengerFactory.getMessenger(type)).thenReturn(messenger);

        SnippetToRefactor snippetToRefactor = new SnippetToRefactor();
        snippetToRefactor.setMessengerFactory(messengerFactory);

        snippetToRefactor.send(type, message);
        verify(messenger).setupConnection();
        verify(messenger).send(message);
        verify(messenger).close();
    }

}
